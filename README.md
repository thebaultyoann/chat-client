# Chat-client 
A simple chat client in UDP or TCP.
Download with 
```
git clone https://gitlab.com/thebaultyoann/udp-chat-client.git
```

## UDP 

## TCP

Go into the folder whith TCP files.
Execute one of the server (choose one of the commands) :
```
java TCPServer #optional : <port>
java TCPMultiServer #optional : <port>
```

Execute a client (once or many)
```
java TCPClient #optional: <ip> <port>
```

Acces to the [TCP JavaDoc](TCP/doc/index.html)
