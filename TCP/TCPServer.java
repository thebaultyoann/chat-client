import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The TCPServer class implements a simple TCP server.
 * This server listens for client connections on a specified port and handles each client connection.
 */
public class TCPServer {
    private final int port;

    /**
     * Constructor to create a TCPServer with a specified port.
     *
     * @param port the port number on which the server will listen
     */
    public TCPServer(int port) {
        this.port = port;
    }

    /**
     * Default constructor which initializes the server on a default port : 33334.
     */
    public TCPServer() {
        this(33334);
    }

    /**
     * Launches the server to start listening for client connections.
     * Handles each client connection by invoking the handleClientConnection method.
     */
    public void launch() {
        ServerSocket serverSocket = null;

        try {
            // Create a server socket that listens on the specified port
            serverSocket = new ServerSocket(port);
            System.out.println("TCP Server is running on port " + port);

            while (true) {
                // Accept a connection from a client
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected: " + clientSocket.getInetAddress());

                // Handling client connection in a separate method
                handleClientConnection(clientSocket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Close the server socket when done
            if (serverSocket != null && !serverSocket.isClosed()) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Handles the communication with a connected client.
     * Reads data from the client, prints it, and echoes it back to the client.
     *
     * @param clientSocket the socket representing the connection to the client
     */
    private void handleClientConnection(Socket clientSocket) {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
             PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)) {
            
            String receivedLine;
            // Read data sent by the client
            while ((receivedLine = in.readLine()) != null) {
                System.out.println("Received from client: " + receivedLine);
                // Echo the received message back to the client
                out.println(receivedLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The main method for running the TCPServer.
     * Accepts an optional port number as a command line argument.
     *
     * @param args command line arguments, expects an optional port number
     */
    public static void main(String[] args) {
        TCPServer server;
        if (args.length > 0) {
            // Use the port constructor if we have an argument
            int port = Integer.parseInt(args[0]);
            server = new TCPServer(port);
        } else {
            // Use the default port else
            server = new TCPServer();
        }
        server.launch();
    }
}
