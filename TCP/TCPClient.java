import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * The TCPClient class establishes a connection to a specified server using TCP/IP protocol
 * and enables communication with the server.
 */
public class TCPClient {
    private final String serverAddress;
    private final int serverPort;

    /**
     * Constructor to create a new TCPClient with specified server address and port.
     *
     * @param serverAddress the IP address of the server
     * @param serverPort the port number of the server
     */
    public TCPClient(String serverAddress, int serverPort) {
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
    }

    /**
     * Default constructor which initializes the client with localhost address and a default port.
     */
    public TCPClient() {
        this("127.0.0.1", 33334); // Localhost IP and default port
    }

    /**
     * Methods that connects to the server and handles the communication.
     * It reads user input from the console, sends it to the server, and displays server responses.
     */
    public void communicateWithServer() {
        try (Socket socket = new Socket(serverAddress, serverPort);
             PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {

            String userInput;
            // Read user input and send to server
            while ((userInput = stdIn.readLine()) != null) {
                out.println(userInput);
                // Read and display the response from the server
                System.out.println("Server says: " + in.readLine());

                // Exit condition (e.g., "exit")
                if ("exit".equalsIgnoreCase(userInput)) {
                    break;
                }
            }
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + serverAddress);
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                serverAddress);
            e.printStackTrace();
        }
    }

    /**
     * The main method for running the TCPClient.
     * It accepts server address and port as command line arguments.
     *
     * @param args command line arguments, expects server address and port number
     */
    public static void main(String[] args) {
        TCPClient client;
        if (args.length >= 2) {
            // Use the arguments for Port and IP
            String serverAddress = args[0];
            int serverPort = Integer.parseInt(args[1]);
            client = new TCPClient(serverAddress, serverPort);
        } else {
            // Use the default constructor if less than two arguments are given
            client = new TCPClient();
        }
        client.communicateWithServer();
    }
}
