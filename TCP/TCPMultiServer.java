import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The TCPMultiServer class represents a multi-threaded TCP server.
 * It can handle multiple client connections simultaneously by spawning a new thread for each connection.
 */
public class TCPMultiServer {
    private final int port;

    /**
     * Constructor to create a TCPMultiServer on a specified port.
     *
     * @param port the port number on which the server will listen for connections
     */
    public TCPMultiServer(int port) {
        this.port = port;
    }

    /**
     * Default constructor which initializes the server on a default port.
     */
    public TCPMultiServer() {
        this(33335); // Default port is set to 33335
    }

    /**
     * Launches the server and continuously listens for client connections.
     * Upon accepting a new connection, it spawns a new thread to handle the client.
     */
    public void launch() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.out.println("TCP MultiServer is running on port " + port);

            while (true) {
                // Accept a new client connection
                Socket clientSocket = serverSocket.accept();
                System.out.println("New client connected: " + clientSocket.getInetAddress());

                // Create and start a new thread for each client
                new ClientHandler(clientSocket).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The main method for running the TCPMultiServer.
     * It accepts a port number as an optional command line argument.
     *
     * @param args command line arguments, expects an optional port number
     */
    public static void main(String[] args) {
        TCPMultiServer server;
        if (args.length > 0) {
            // Use the port constructor if we have an argument 
            int port = Integer.parseInt(args[0]);
            server = new TCPMultiServer(port);
        } else {
           // Use the default port else
            server = new TCPMultiServer();
        }
        server.launch();
    }

    /**
     * The ClientHandler class is an inner class of TCPMultiServer.
     * It handles communication with a connected client on a separate thread.
     */
    private static class ClientHandler extends Thread {
        private final Socket clientSocket;

        // Constructor for ClientHandler
        public ClientHandler(Socket socket) {
            this.clientSocket = socket;
        }

        /**
         * The run method is invoked when the thread starts.
         * It handles reading and writing data to/from the client.
         */
        @Override
        public void run() {
            try (BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                 PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true)) {

                String inputLine;
                // Read data sent by the client and echo it back
                while ((inputLine = in.readLine()) != null) {
                    System.out.println("Received from client: " + inputLine);
                    out.println(inputLine);
                }
                // Catch Execeptions
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
