import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.charset.StandardCharsets;

public class UDPClient {
    private final String serverAddress;
    private final int serverPort;

    // Constructor to set the server address and port
    public UDPClient(String serverAddress, int serverPort) {
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
    }

    // Send data to the server
    public void sendToServer() throws IOException {
        try (DatagramSocket socket = new DatagramSocket();
             BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            InetAddress address = InetAddress.getByName(serverAddress);

            while (true) {
                System.out.print("Enter message: ");
                String message = reader.readLine();
                byte[] buffer = message.getBytes(StandardCharsets.UTF_8);

                // Create and send the packet to server
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, serverPort);
                socket.send(packet);

                // Break the loop if the user inputs "exit"
                if ("exit".equalsIgnoreCase(message)) {
                    break;
                }
            }
        }
    }

    // Main method for execution
    public static void main(String[] args) throws IOException {
        UDPClient client = new UDPClient("localhost", 33333); // Server address and port
        client.sendToServer();
    }
}
