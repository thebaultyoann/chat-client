import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;

public class UDPServer {
    private DatagramSocket socket;
    private final int port;

    // Constructor with port
    public UDPServer(int port) {
        this.port = port;
    }

    // Default constructor
    public UDPServer() {
        this(33333); // Sets the default port to 33333
    }

    // Method to launch the UDP server
    public void launch() {
        try {
            // Create a socket to listen on the specified port
            socket = new DatagramSocket(port);
            System.out.println("UDP Server is running on port " + port);

            while (true) {
                // Buffer to store incoming data
                byte[] buffer = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

                // Receive packet from a client
                socket.receive(packet);

                // Convert packet data to string using UTF-8 encoding
                String received = new String(packet.getData(), 0, packet.getLength(), StandardCharsets.UTF_8);
                System.out.println("Received: " + received);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Close the socket when done
            if (socket != null && !socket.isClosed()) {
                socket.close();
            }
        }
    }

    // Main method for execution
    public static void main(String[] args) {
        UDPServer server = new UDPServer();
        server.launch();
    }

    @Override
    public String toString() {
        return "UDPServer{" +
                "port=" + port +
                '}';
    }
}
